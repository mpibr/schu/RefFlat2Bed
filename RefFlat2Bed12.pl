#!/usr/bin/perl

use warnings;
use strict;

my $rfl_file = shift;

open(my $fh, "<", $rfl_file) or die $!;
while (<$fh>)
{
    chomp($_);
    
    my ($gene, $transcript, $chrom, $strand, $chrom_start, $chrom_end, $thick_start, $thick_end, $blocks, $block_start, $block_end) = split("\t", $_, 11);
    
    my $name = $transcript . ";" . $gene;
    my @blockStarts = split(",", $block_start);
    my @blockEnd = split(",", $block_end);
    
    my @exonStarts = ();
    my @exonSizes = ();
    for(my $ex = 0; $ex < $blocks; $ex++)
    {
        push(@exonStarts, $blockStarts[$ex] - $chrom_start);
        push(@exonSizes, $blockEnd[$ex] - $blockStarts[$ex]);
    }
    
    print $chrom,"\t",$chrom_start,"\t",$chrom_end,"\t",$name,"\t",0,"\t",$strand,"\t",$thick_start,"\t",$thick_end,"\t",0,"\t",$blocks,"\t",join(",",@exonSizes),"\t",join(",",@exonStarts),"\n";
    
    
}
close($fh);





